# Ansible playbook deployment backup

For using playbook:

* We have to change in backup-rftplimit.yml IP of some range of addresses of servers for deploy
* Copy and edit file roles/backup-rftplimit/templates/settings.default.cfg to roles/backup-rftplimit/templates/settings.hostname.cfg, where hostname in settings.hostname.cfg is a hostname of destination host
* Run the next command:

ansible-playbook -i hosts backup-rftplimit.yml

Advanced deployment:

* Configure playbook backup-rftplimit2.yml , change parametres of command shell
* Run the command:
ansible-playbook -i hosts backup-rftplimit2.yml
