# ansible 164.132.90.29 -m shell -a 'cat /root/bin/HH-backup/scinstall.sh'

#!/bin/bash



function check_program()
{
if which ${1}; then
	echo "package ${1} is installed"
else
	echo "package is not install. Please install package ${1}";
	exit
fi
}

check_program "git"
check_program "nice"
check_program "mail"
check_program "curlftpfs"

# clone repository

cd /root/bin/
git clone https://hhosting@bitbucket.org/holbihosting/dedicated_backup.git/HH-backup

cd HH-backup
cp settings.sample.cfg settings.cfg
chmod 600 settings.cfg

# install crontabs

if grep "0 2 * * * /usr/bin/nice -n 10 /root/bin/HH-backup/backup.sh" /var/spool/cron/crontabs/root; then
	echo "Entry already in crontab";
else
	echo "0 2 * * * /usr/bin/nice -n 10 /root/bin/HH-backup/backup.sh" >> /var/spool/cron/crontabs/root;
fi

if grep "0 3 * * * /usr/bin/nice -n 10 /root/bin/HH-backup/sync_ftp.sh" /var/spool/cron/crontabs/root; then
        echo "Entry already in crontab";
else
        echo "0 3 * * * /usr/bin/nice -n 10 /root/bin/HH-backup/sync_ftp.sh" >> /var/spool/cron/crontabs/root;
fi
